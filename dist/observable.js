(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ArrayPrototype = Array.prototype;

var ArrayObservable = (function (_Array) {
  _inherits(ArrayObservable, _Array);

  function ArrayObservable() {
    _classCallCheck(this, ArrayObservable);

    _get(Object.getPrototypeOf(ArrayObservable.prototype), 'constructor', this).apply(this, arguments);
  }

  _createClass(ArrayObservable, [{
    key: 'fire',

    // constructor () {
    //     super();
    // }

    /**
     * Fire change
     */
    value: function fire(change) {

      change.object = this;
      // change.path = '...';

      console.log(change);
    }

    /**
     * Set value
     *
     * `array[0] = 'foo'` becomes `array.set(0, 'foo')`
     */
  }, {
    key: 'set',
    value: function set(index, newValue) {

      if (index !== index >> 0) {
        throw 'Array.set(' + index + ', ' + newValue + ') index is not an integer';
      }

      var length = this.length;
      var oldValue = this[index];

      if (index >= length) {

        var addedCount = index - length;

        // this[index] = value does not work.. (when extending Array?)
        // With push all works fine
        for (var i = 0; i < addedCount; i++) {
          ArrayPrototype.push.call(this, undefined);
        }

        ArrayPrototype.push.call(this, newValue);

        this.fire({
          type: 'splice',
          index: length,
          // value: value,
          removed: [],
          addedCount: addedCount
        });
      } else {

        this[index] = newValue;

        this.fire({
          type: 'update',
          index: index,
          oldValue: oldValue
        });
      }
    }

    // --- Array methods --- //

    /**
     * Joins two or more arrays, and returns a copy of the joined arrays
     */
  }, {
    key: 'concat',
    value: function concat() {
      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return new WatchableArray(Array.prototype.concat.apply(this.toArray(), args));
    }

    /**
     * Removes the last element of an array, and returns that element
     */
  }, {
    key: 'pop',
    value: function pop() {
      var value = Array.prototype.pop.apply(this);

      this.fire({
        type: 'delete',
        index: this.length
      });

      return value;
    }

    /**
     * Adds new elements to the end of an array, and returns the new length
     */
  }, {
    key: 'push',
    value: function push(value) {
      var length = ArrayPrototype.push.call(this, value);

      this.fire({
        type: 'splice',
        index: length - 1,
        // value: value,
        removed: [],
        addedCount: 1
      });

      return length;
    }

    /**
     * Reverses the order of the elements in an array
     */
  }, {
    key: 'reverse',
    value: function reverse() {
      ArrayPrototype.reverse.call(this);

      // Should fire update events for every change
      // name [euh should be index?], object, oldValue, type

      this.fire({
        type: 'reverse'
      });
    }

    /**
     * Removes the first element of an array, and returns that element
     */
  }, {
    key: 'shift',
    value: function shift() {
      var value = ArrayPrototype.reverse.call(this);

      this.fire({
        type: 'splice',
        index: 0,
        // value: value,
        removed: [],
        addedCount: 0
      });

      return value;
    }

    /**
     * Selects a part of an array, and returns the new array
     */
    // slice ( begin, end ) {
    //     return ArrayPrototype.slice.call(this, begin, end);
    // }

    /**
     * Sorts the elements of an array
     */
  }, {
    key: 'sort',
    value: function sort(compareFunction) {
      return ArrayPrototype.sort.call(this, compareFunction);
    }

    /**
     * Adds/Removes elements from an array
     */
  }, {
    key: 'splice',
    value: function splice() {
      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        args[_key2] = arguments[_key2];
      }

      var removed = ArrayPrototype.splice.apply(this, args);

      this.fire({
        type: 'splice',
        index: 0,
        // value: args.slice(2),
        removed: removed,
        addedCount: args.length - 2
      });

      return removed;
    }

    /**
     * Adds new elements to the beginning of an array, and returns the new length
     */
  }, {
    key: 'unshift',
    value: function unshift() {
      return ArrayPrototype.slice.call(this);
    }
  }]);

  return ArrayObservable;
})(Array);

exports.ArrayObservable = ArrayObservable;

},{}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _arrayObservable = require('./arrayObservable');

var _objectObservable = require('./objectObservable');

// @todo(Saar) exports trough window isnt that nice..
window.ArrayObservable = _arrayObservable.ArrayObservable;
window.ObjectObservable = _objectObservable.ObjectObservable;

exports.ArrayObservable = _arrayObservable.ArrayObservable;
exports.ObjectObservable = _objectObservable.ObjectObservable;

},{"./arrayObservable":1,"./objectObservable":3}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ObjectObservable = (function (_Object) {
    _inherits(ObjectObservable, _Object);

    function ObjectObservable() {
        _classCallCheck(this, ObjectObservable);

        _get(Object.getPrototypeOf(ObjectObservable.prototype), "constructor", this).call(this);
    }

    _createClass(ObjectObservable, [{
        key: "fire",
        value: function fire(change) {

            change.object = this;
            // change.path = '...';

            console.log(change);
        }
    }, {
        key: "set",
        value: function set(key, value) {}
    }]);

    return ObjectObservable;
})(Object);

exports.ObjectObservable = ObjectObservable;

},{}]},{},[2])


//# sourceMappingURL=observable.js.map