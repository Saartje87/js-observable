const ArrayPrototype = Array.prototype;

class ArrayObservable extends Array {

    // constructor () {
    //     super();
    // }

    /**
     * Fire change
     */
    fire ( change ) {

		change.object = this;
		// change.path = '...';

		console.log(change);
	}

    /**
     * Set value
     *
     * `array[0] = 'foo'` becomes `array.set(0, 'foo')`
     */
    set ( index, newValue ) {

        if( index !== index >> 0 ) {
            throw `Array.set(${index}, ${newValue}) index is not an integer`;
        }

        const length = this.length;
        const oldValue = this[index];

        if( index >= length ) {

            const addedCount = index - length;

            // this[index] = value does not work.. (when extending Array?)
            // With push all works fine
            for( let i = 0; i < addedCount; i++ ) {
                ArrayPrototype.push.call(this, undefined);
            }

            ArrayPrototype.push.call(this, newValue);

            this.fire({
    			type: 'splice',
    			index: length,
    			// value: value,
                removed: [],
                addedCount: addedCount
    		});
        } else {

            this[index] = newValue;

            this.fire({
    			type: 'update',
                index: index,
                oldValue: oldValue
    		});
        }
    }

    // --- Array methods --- //

    /**
     * Joins two or more arrays, and returns a copy of the joined arrays
     */
    concat ( ...args ) {
        return new WatchableArray(Array.prototype.concat.apply(this.toArray(), args));
    }

    /**
     * Removes the last element of an array, and returns that element
     */
    pop () {
        let value = Array.prototype.pop.apply(this);

		this.fire({
			type: 'delete',
			index: this.length
		});

		return value;
    }

    /**
     * Adds new elements to the end of an array, and returns the new length
     */
    push ( value ) {
        const length = ArrayPrototype.push.call(this, value);

        this.fire({
			type: 'splice',
			index: length - 1,
			// value: value,
            removed: [],
            addedCount: 1
		});

        return length;
    }

    /**
     * Reverses the order of the elements in an array
     */
    reverse () {
        ArrayPrototype.reverse.call(this);

        // Should fire update events for every change
        // name [euh should be index?], object, oldValue, type

        this.fire({
			type: 'reverse'
		});
    }

    /**
     * Removes the first element of an array, and returns that element
     */
    shift () {
        const value = ArrayPrototype.reverse.call(this);

        this.fire({
			type: 'splice',
			index: 0,
			// value: value,
            removed: [],
            addedCount: 0
		});

        return value;
    }

    /**
     * Selects a part of an array, and returns the new array
     */
    // slice ( begin, end ) {
    //     return ArrayPrototype.slice.call(this, begin, end);
    // }

    /**
     * Sorts the elements of an array
     */
    sort ( compareFunction ) {
        return ArrayPrototype.sort.call(this, compareFunction);
    }

    /**
     * Adds/Removes elements from an array
     */
    splice ( ...args ) {
        const removed = ArrayPrototype.splice.apply(this, args);

        this.fire({
			type: 'splice',
			index: 0,
			// value: args.slice(2),
            removed: removed,
            addedCount: args.length - 2
		});

        return removed;
    }

    /**
     * Adds new elements to the beginning of an array, and returns the new length
     */
    unshift () {
        return ArrayPrototype.slice.call(this);
    }
}

export {ArrayObservable};
