import {ArrayObservable} from './arrayObservable';
import {ObjectObservable} from './objectObservable';

// @todo(Saar) exports trough window isnt that nice..
window.ArrayObservable = ArrayObservable;
window.ObjectObservable = ObjectObservable;

export {ArrayObservable, ObjectObservable};
